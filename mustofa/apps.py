from django.apps import AppConfig


class MustofaConfig(AppConfig):
    name = 'mustofa'
