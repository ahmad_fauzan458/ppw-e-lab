from django.shortcuts import render
from datetime import datetime, date

birth_date = date(1998, 6, 21)
mhs_name = 'Ahmad Mustofa Halim'
npm = '1706043512'
gender = 'Male'
hobby = 'Teknologi'
college = 'Computer Science, UI'
description = 'Mustofa adalah orang yang baik hati'

bio_dict = [{'subject' : 'Name', 'value' : mhs_name},\
{'subject' : 'Birth Date', 'value' : birth_date.strftime('%d %B %Y')},\
{'subject' : 'Gender', 'value' : gender},\
{'subject' : 'NPM', 'value' : npm},\
{'subject' : 'Hobby', 'value' : hobby},\
{'subject' : 'College', 'value' : college},\
{'subject' : 'Description', 'value' : description}]


def index(request):
 response = {'bio_dict' : bio_dict}
 return render(request, 'description_mustofa.html', response)