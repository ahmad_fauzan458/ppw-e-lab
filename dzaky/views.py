from django.shortcuts import render
from datetime import datetime, date

birth_date = date(1999, 9, 17)
mhs_name = 'Dzaky Abdi Al Jabbar'
npm = '1706043462'
gender = 'Male'
hobby = 'Pencak Silat'
college = 'Computer Science, UI'
description = 'Dzakky suka bela diri, terutama pencak silat'

bio_dict = [{'subject' : 'Name', 'value' : mhs_name},\
{'subject' : 'Birth Date', 'value' : birth_date.strftime('%d %B %Y')},\
{'subject' : 'Gender', 'value' : gender},\
{'subject' : 'NPM', 'value' : npm},\
{'subject' : 'Hobby', 'value' : hobby},\
{'subject' : 'College', 'value' : college},\
{'subject' : 'Description', 'value' : description}]


def index(request):
 response = {'bio_dict' : bio_dict}
 return render(request, 'description_dzaky.html', response)